import 'package:flutter/material.dart';
import 'package:quizzler/quiz_brain.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

QuizBrain quizBrain = QuizBrain();

void main() => runApp(Quizzler());

// STATELESS
class Quizzler extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.grey[900],
        body: SafeArea(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.0),
            child: QuizPage(),
          ),
        ),
      ),
    );
  }
}

// STATEFUL
class QuizPage extends StatefulWidget {
  @override
  _QuizPageState createState() => _QuizPageState();
}

class _QuizPageState extends State<QuizPage> {
  List<Icon> scoreKeeper = [];

  int qNumber = 0;

  Expanded displayQuestion(qNumber) {
    return Expanded(
      flex: 5,
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Center(
          child: Text(
            quizBrain.getQuestionText(),
            textAlign: TextAlign.center,
            style: TextStyle(
                color: Colors.white,
                fontSize: 25.0,
                fontWeight: FontWeight.bold),
          ),
        ),
      ),
    );
  }

  void displayResult(icon, color) {
    scoreKeeper.add(
      Icon(
        icon,
        color: color,
      ),
    );
  }

  void checkAnswer(userPickedAnswer) {
    setState(() {
      if (quizBrain.isFinished()) {
        Alert(
          context: context,
          title: "Finished!",
          desc: "You've reached the end of the quiz.",
          buttons: [
            DialogButton(
              child: Text(
                "OKAY",
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              onPressed: () => Navigator.pop(context),
              width: 120,
            )
          ],
        ).show();
        quizBrain.reset();
        scoreKeeper = [];
      } else {
        bool correctAnswer = quizBrain.getQuestionAnswer();

        if (correctAnswer == userPickedAnswer) {
          displayResult(Icons.check, Colors.green);
        } else {
          displayResult(Icons.close, Colors.red);
        }

        quizBrain.nextQuestion();
      }
    });
  }

  Expanded displayButton(color, userPickedAnswer) {
    return Expanded(
      child: Container(
        padding: EdgeInsets.all(15.0),
        child: FlatButton(
          color: color,
          child: Text(
            userPickedAnswer,
            style: TextStyle(color: Colors.white, fontSize: 20.0),
          ),
          onPressed: () {
            if (userPickedAnswer == 'True')
              checkAnswer(true);
            else
              checkAnswer(false);
          },
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        displayQuestion(qNumber),
        displayButton(Colors.green, 'True'),
        displayButton(Colors.red, 'False'),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: scoreKeeper,
          ),
        ),
      ],
    );
  }
}
