class Question {
  String qText;
  bool qAnswer;

  // Constructor
  Question(String q, bool a) {
    qText = q;
    qAnswer = a;
  }
}
